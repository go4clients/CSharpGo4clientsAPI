﻿using System;
using RestSharp;

namespace Rest
{
	public class VoiceClass
	{
		//Please put your Apikey and Apisecret there

		string apisecret ="";
		string apikey="";

		public VoiceClass ()
		{
		}

		public void sentCall(){
			Console.Write ("the CAll will be Send !!");
			var client = new RestClient("https://go4clients.com/TelintelSms/api/voice/outcall");
			var request = new RestRequest(Method.POST);

			request.AddHeader("apisecret",apisecret);
			request.AddHeader("apikey",apikey);
			request.AddHeader("content-type", "application/json");
			request.AddParameter("application/json", "{\n  \"from\": \"17868716544\",\n  \"toList\": [\"572222222\",\"573222222\"],\n  \"callSteps\": [\n  {\n \"type\": \"SAY\",\n      \"text\": \"Hola por favor presiona 1 para soporte o presiona 2 para redireccionar la llamada a compensar\",\n      \"voice\": \"CARLOS\",\n      \"sourceType\":\"STANDARD\"\n    },\n {\n \"type\": \"DETECT\",\n \n  \"options\": [\n  {\n\"type\": \"OPTION\",\n  \"dtfmCommand\": \"1\",\n \"optionId\": \"soporte\",\n  \"steps\": [\n {\n  \"type\": \"SAY\",\n  \"text\": \"lo estamos comunicando con Soporte\",\n \"voice\": \"CARLOS\",\n \"sourceType\":\"STANDARD\"\n  },\n   {\n \"destination\": \"573203954702\",\n \"type\": \"DIAL\"\n}\n]\n},\n {\n \"type\": \"OPTION\",\n \"dtfmCommand\": \"2\",\n \"optionId\": \"soporte\",\n \"steps\": [\n {\n \"type\": \"SAY\",\n\"text\": \"lo estamos comunicando con Compensar\",\n \"voice\": \"CARLOS\",\n \"sourceType\":\"STANDARD\"\n },\n {\n \"destination\": \"5714441234\",\n \"type\": \"DIAL\"\n }\n ]\n }\n ]\n }\n ]\n}", ParameterType.RequestBody);
			IRestResponse response = client.Execute(request);

			var content = response.Content;

			Console.Write (content);
		}
	}
}

